BACKEND: 
	- Facilitate the seamless submission of corresponding subtitle files by translators for each video hosted on MOOC sites, as well as the upload of translated documents. This functionality empowers both translators and administrators to efficiently oversee and maintain translation and subtitle-related data.
	- A comprehensive database encompasses a curated collection of subjects, video links, and accompanying visit metrics for each video.

a) Translator:
	- Upload the translated Vietnamese subtitle file:
		+ Access the upload screen and provide necessary information such as Video Name, Video URL, Course ID, and Video ID.
		+ Attach the translated subtitle file.
		+ Upon clicking submit, the application will facilitate the upload and storage of data in the database.

	- Upload translated Document file in Vietnamese:
		+ Input the Translation Name and URL of the Document.
		+ Attach the translated Document file.

	- View the list of uploaded translation files: 
		+ Two Dashboards will be available, one displaying subtitle translations and the other displaying Document translations.

	- Download the translated file:
		+ By clicking on the link provided on the Dashboard, users can download the translation data, including the Subtitle file or Document file.

	- Download the translated file:
		+ By clicking on the link provided on the Dashboard, users can download the translation data, including the Subtitle file or Document file.

	- Search for translations using filters:
		+ Have the ability to search for translations using different filters such as Translation name, URL, Translator, Course ID, and Video ID.

	- Delete subtitle files or documents:
		+ Can choose to delete that particular translation.

	- Edit information:
		+ Can opt to make edits to the information associated with that translation.
		+ Two types of editing are available: Edit the metadata of the translation file or Re-upload a different translation file in place of the original.

b) Administrator:
	- Primarily responsible for user account management. 
	-  Deletion of user accounts.
	-  Granting permissions to a user (transferring the role from User to Translator).
	-  Revoking permissions from a user (reverting the role from Translator to User).

b) Reviewer
	- View the list of uploaded translation files: 
		+ Two Dashboards will be available, one displaying subtitle translations and the other displaying Document translations.

	- Search for translations using filters:
		+ Have the ability to search for translations using different filters such as Translation name, URL, Translator, Course ID, and Video ID.
	
	- Entrusted with the responsibility of meticulously assessing the precision of the Translator's video translations and providing constructive feedback.

	- Download the translated file:
		+ By clicking on the link provided on the Dashboard, users can download the translation data, including the Subtitle file or Document file.

